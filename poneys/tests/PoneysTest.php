<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {
    private $Poneys;

    public function setUp(){
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(8);
    }
    public function test_removePoneyFromField() {
      
      
      

      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());
    }


    public function test_addPoney() {
      
      //Action
	    $this->Poneys->addPoney(2);
      //Assert
 	    $this->assertEquals(10, $this->Poneys->getCount());
    }


    public function test_removeException() {
     
      try {
        $this->Poneys->removeException(10);
       }catch (Exception $e) {
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
       } 
    }

    public function test_getNames()
    {
      $poneys = $this->getMockBuilder('Poneys')->getMock();
      $poneys->method('getNames')->willreturn(['Youss','Mouss','Ciss']);
      $this->assertContains('Ciss', $poneys->getNames());
          
    }

    public function tearDown() {
      unset($this->poneys);
    }

  }
 ?>
